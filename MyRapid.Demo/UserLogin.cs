﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MyRapid.Proxy;
using MyRapid.Code;
using System.Diagnostics;
using DevExpress.XtraSplashScreen;
using MyRapid.Proxy.MainService;

namespace MyRapid.Client
{
    public partial class UserLogin : DevExpress.XtraEditors.XtraForm
    {
 
        public UserLogin()
        {
            InitializeComponent();

            string p = Configuration.ReadSetting("product");
            if (!string.IsNullOrEmpty(p))
            {
                labelControl3.Text = p;
            }

        }
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            
            try
            {
                if (string.IsNullOrEmpty(textEdit1.Text))
                {
                    textEdit1.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(textEdit2.Text))
                {
                    textEdit2.Focus();
                    return;
                }
                string token = BaseService.Login(textEdit1.Text, Encrypt.TokenEncrypt(Encrypt.MD5Encrypt(textEdit2.Text)));
                if (string.IsNullOrEmpty(token))
                {
                    XtraMessageBox.Show("用户名不存在或密码错误！");
                    return;
                }

                Configuration.SaveSetting("lastLogin", textEdit1.Text);
                this.Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("用户名不存在或密码错误！\r\n" + ex.Message);
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
 
            this.Close();
        }


        private void MoveHandle(Control ctrl)
        {
            Point sourcePoint = new Point(0, 0);
            bool isMove = false;
            ctrl.MouseDown += delegate (object sender, MouseEventArgs e)
            {
                sourcePoint = e.Location;
                isMove = true;
            };

            ctrl.MouseMove += delegate (object sender, MouseEventArgs e)
            {
                if (isMove)
                    this.Location = new Point(this.Location.X + e.X - sourcePoint.X, this.Location.Y + e.Y - sourcePoint.Y);
            };

            ctrl.MouseUp += delegate (object sender, MouseEventArgs e)
            {
                isMove = false;
            };
        }

        private void UserLogin_Shown(object sender, EventArgs e)
        {
            //MoveHandle(pictureEdit1);
            MoveHandle(labelControl1);
            MoveHandle(labelControl2);
            MoveHandle(labelControl3);
            MoveHandle(labelControl4);
            MoveHandle(this);

            this.TopMost = true;
            this.TopMost = false;
            this.Activate();
            string u = Configuration.ReadSetting("lastLogin");
            if (!string.IsNullOrEmpty(u))
            {
                textEdit1.Text = u;
                textEdit2.Focus();
            }
        }

        private void labelControl3_Click(object sender, EventArgs e)
        {

        }
    }
}