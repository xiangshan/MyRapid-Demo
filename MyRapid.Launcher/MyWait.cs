/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
namespace MyRapid.Launcher
{
    public partial class MyWait : Form
    {
        //公共参数
        string exePath = "MyRapid.Client.exe";
        string vbsPath = "update.vbs";  //更新脚本放在服务端 客户端下载后执行
        string imgPath = ""; //可以配置自定义图片路径  也可以写到配置文件
        string exeTitle = "用户登录";  //软件除了更新还是一个启动等待画面  等待到这个Title的窗体自动退出等待画面
        string urlPath = "http://127.0.0.1:4824/update.txt";  //更新文件配置文本   用txt虽然麻烦点 但是 可以避免使用类库
        int WaitSecond = 30;  //等待时间  更新后等待30秒后还是没有找到指定的窗体 也退出

        Process process;
        Stopwatch sw = new Stopwatch();

        public MyWait()
        {
            sw.Restart();
            InitializeComponent();
            //小贴士
            if (File.Exists("tips.txt"))
            {
                tips = File.ReadAllLines("tips.txt");
            }
        }

        private void UpdateFile()
        {
            try
            {
                string oldVersion = ConfigurationManager.AppSettings["version"];
                DateTime ver = DateTime.Parse(oldVersion);
                WebClient wc = new WebClient();
                wc.Encoding = Encoding.UTF8;
                label1.Text = "正在检查更新...";
                string updateFile = "update.txt";
                wc.DownloadFile(urlPath, updateFile);
                if (!File.Exists(updateFile)) return;
                string[] verString = File.ReadAllLines(updateFile);
                if (verString.Length == 0) return;
                if (DateTime.Parse(verString[0]) <= ver) return;

                string fName = Path.GetFileNameWithoutExtension(exePath);
                fName = Path.GetFileName(exePath);
                var prcList = Process.GetProcesses().Where(pr => pr.ProcessName == Path.GetFileNameWithoutExtension(exePath) || pr.ProcessName == Path.GetFileName(exePath));
                foreach (var pk in prcList)
                {
                    try { pk.Kill(); }
                    catch
                    {   // 进程被保护而抛出异常(可以使用其它手段,如
                    }
                }

                progressBar1.Maximum = verString.Length;

                foreach (string sf in verString)
                {
                    //Tips
                    if (tips != null && tips.Length > 0 && sw.Elapsed.TotalSeconds % 4 < 1)
                    {
                        int i = DateTime.Now.Millisecond % (tips.Length - 1);
                        label3.Text = string.Format(tip, tips[i]);
                    }

                    if (progressBar1.Value < verString.Length)
                        progressBar1.Value += 1;
                    if (!sf.Contains("|")) continue;
                    string[] ups = sf.Split('|');

                    if (DateTime.Parse(ups[0]) <= ver) continue;


                    //如果目录不存在这创建
                    string fDir = Path.GetDirectoryName(ups[1]);
                    if (!string.IsNullOrEmpty(fDir) && !Directory.Exists(fDir))
                    {
                        Directory.CreateDirectory(fDir);
                    }
                    //下载文件
                    label1.Text = ups[1];
                    this.Refresh();
                    wc.DownloadFile(ups[2], ups[1]);
                }

                //记录更新时间
                Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                configuration.AppSettings.Settings["version"].Value = DateTime.Now.ToString();
                configuration.Save(ConfigurationSaveMode.Modified);

                label1.Text = "更新结束：正在启动主程序...";
                progressBar1.Style = ProgressBarStyle.Marquee;
            }
            catch (Exception ex)
            {
                label1.Text += "更新失败，请重试或联系管理员协助处理：" + ex.Message;
            }
        }

        private void StartMain()
        {
            //执行预更新脚本
            if (File.Exists(vbsPath))
            {
                if (vbsPath.EndsWith(".vbs"))
                {
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.FileName = "wscript.exe";
                    startInfo.Arguments = vbsPath;
                    Process.Start(startInfo);
                }
                else
                {
                    Process script = Process.Start(vbsPath);
                }

            }
            if (File.Exists(exePath))
            {
                //启动程序
                ProcessStartInfo processStartInfo = new ProcessStartInfo();
                processStartInfo.FileName = exePath;
                processStartInfo.WorkingDirectory = Path.GetDirectoryName(exePath);
                process = Process.Start(processStartInfo);
            }
            else
            {
                Environment.Exit(0);
            }
            if (File.Exists(imgPath))
            {
                //加载动画
                Image image = Image.FromFile(imgPath);
                pictureBox1.Image = image;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //这里是用于判断结束 分两种  超时  或  已完成
            if (sw.Elapsed.TotalSeconds > WaitSecond)
                Environment.Exit(0);
            if (process == null)
                Environment.Exit(0);
            if (process.HasExited)
                Environment.Exit(0);
            process.Refresh();
            if (process.MainWindowTitle.Equals(exeTitle))
                Environment.Exit(0);



            //Console.WriteLine(process.MainWindowTitle);
            //Console.WriteLine(sw.Elapsed.TotalMilliseconds);
        }

        private void MyWait_Shown(object sender, EventArgs e)
        {
            this.Refresh();
            UpdateFile();
            if (File.Exists("update.txt"))
                File.Delete("update.txt");
            sw.Restart();
            timer1.Enabled = true;
            StartMain();
        }

        #region Tips
        string[] tips;
        string tip = "小贴士:{0}";

        #endregion




    }
}